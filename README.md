Powernim
======
## Simple GTK power menu for linux (systemd)
![Alt text](https://codeberg.org/wreed/powernim/raw/branch/master/assets/sample.png)

Build instructions
------
You will need [Nim](https://nimlang.org) to build/install Powernim
I'd recommend using [choosenim](https://github.com/dom96/choosenim) for this, or your system package manager if it's available.

```
nimble check
nimble build

./powernim
```
If you wish to install it, run `nimble install` : this will put the binary in your `~/.nimble/bin` - but you can alternatively just put the binary in your `/usr/bin` or `/usr/local/bin`

## Contact
------
### [Email Me](mailto:wreedb@yandex.com)